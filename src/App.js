import React from "react";
import "./App.css";
import theme from "./theme";
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import routes from "./config/routes";
import { ThemeProvider } from "@material-ui/core/styles";
import Home from "./pages/Home";
import Hotels from "./pages/Hotels";
import Hotel from "./pages/Hotel";
import Header from "./components/Header";
import Login from "./pages/Login/Login";
import Register from "./pages/Register";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
      <Header/>
        <Switch>
          <Route exact path={routes.home}>
            <Home />
          </Route>
          <Route exact path={routes.hotels}>
            <Hotels />
          </Route>
          <Route exact path={routes.login}>
            <Login />
          </Route>
          <Route exact path={routes.register}>
            <Register />
          </Route>
          <Route exact path={routes.hotel}>
            <Hotel />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
