import React from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";
import {
  Container,
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
} from "@material-ui/core";
import { Typography } from "@material-ui/core";
import routes from "../../config/routes";
import "./Register.css";
import { useTranslation } from "react-i18next";

const Register = () => {
  const { t } = useTranslation("common");
  const history = useHistory();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = async ({
    name,
    lastname,
    email,
    password,
    username,
    address,
    phoneNumber,
    idNumber,
    city,
  }) => {
    // const data = await register(name, lastname, email, password, username, address, phoneNumber, idNumber, city);
    document.location = "/hotels";
  };

  return (
    <Container maxWidth="md">
      <div className="register__container">
        <Typography variant="h3" gutterBottom color="primary">
          {t("register.title")}
        </Typography>
        <form onSubmit={handleSubmit(onSubmit)} className="register__form">
          <div>
            <TextField
              label={t("register.name")}
              color="primary"
              type="text"
              {...register("name", {
                required: t('errorControl.required'),
                maxLength: {
                  value: 80,
                  message: t('errorControl.max80'),
                },
              })}
            />
            {errors.name && (
              <p style={{ color: "red" }}>{errors.name.message}</p>
            )}
          </div>
          <div>
            <TextField
              label={t("register.lastname")}
              color="primary"
              type="text"
              {...register("lastname", {
                required: t('errorControl.required'),
                maxLength: {
                  value: 100,
                  message: t('errorControl.max100'),
                },
              })}
            />
            {errors.lastname && (
              <p style={{ color: "red" }}>{errors.lastname.message}</p>
            )}
          </div>
          <div>
            <TextField
              label="Email"
              color="primary"
              type="text"
              {...register("email", {
                required: t('errorControl.required'),
                pattern: {
                  value: /^\S+@\S+$/i,
                  message: t('errorControl.email'),
                },
              })}
            />
            {errors.email && (
              <p style={{ color: "red" }}>{errors.email.message}</p>
            )}
          </div>
          <div>
            <TextField
              label={t("register.password")}
              color="primary"
              type="password"
              {...register("password", {
                required: t('errorControl.required'),
                minLength: {
                  value: 8,
                  message: t('errorControl.password'),
                },
              })}
            />
            {errors.password && (
              <p style={{ color: "red" }}>{errors.password.message}</p>
            )}
          </div>
          <div>
            <TextField
              label={t("register.username")}
              color="primary"
              type="text"
              {...register("username", {
                required: t('errorControl.required'),
                maxLength: {
                  value: 10,
                  message: t('errorControl.max10'),
                },
              })}
            />
            {errors.username && (
              <p style={{ color: "red" }}>{errors.username.message}</p>
            )}
          </div>
          <div>
            <TextField
              label={t("register.address")}
              color="primary"
              type="text"
              {...register("address", {
                required: t('errorControl.required'),
                maxLength: {
                  value: 100,
                  message: t('errorControl.max100'),
                },
              })}
            />
            {errors.address && (
              <p style={{ color: "red" }}>{errors.address.message}</p>
            )}
          </div>
          <div>
            <TextField
              label={t("register.phoneNumber")}
              color="primary"
              type="tel"
              {...register("phoneNumber", {
                required: t('errorControl.required'),
                maxLength: {
                  value: 9,
                  message: t('errorControl.max9'),
                },
                pattern: {
                  value: /(\+34|0034|34)?[ -]*(6|7)[ -]*([0-9][ -]*){8}/,
                  message: t('errorControl.phoneNumber'),
                },
              })}
            />
            {errors.phoneNumber && (
              <p style={{ color: "red" }}>{errors.phoneNumber.message}</p>
            )}
          </div>
          <div>
            <TextField
              label="DNI"
              color="primary"
              type="text"
              {...register("idNumber", {
                required: t('errorControl.required'),
                maxLength: {
                  value: 9,
                  message: t('errorControl.max9'),
                },
                pattern: {
                  value: /^[0-9]{8,8}[A-Za-z]$/,
                  message: t('errorControl.id'),
                },
              })}
            />
            {errors.idNumber && (
              <p style={{ color: "red" }}>{errors.idNumber.message}</p>
            )}
          </div>
          <div style={{ marginBottom: "20px" }}>
            <FormControl>
              <InputLabel id="demo-simple-select-helper-label">
                {t("register.city")}
              </InputLabel>
              <Select
                labelId="demo-simple-select-helper-label"
                {...register("city", { required: t('errorControl.required') })}
              >
                <MenuItem value="A Coruña">A Coruña</MenuItem>
                <MenuItem value="Alava">Alava</MenuItem>
                <MenuItem value="Albacete">Albacete</MenuItem>
                <MenuItem value="Alicante">Alicante</MenuItem>
                <MenuItem value="Almería">Almería</MenuItem>
                <MenuItem value="Asturias">Asturias</MenuItem>
                <MenuItem value="Avila">Avila</MenuItem>
                <MenuItem value="Badajoz">Badajoz</MenuItem>
                <MenuItem value="Barcelona">Barcelona</MenuItem>
                <MenuItem value="Burgos">Burgos</MenuItem>
                <MenuItem value="Cáceres">Cáceres</MenuItem>
                <MenuItem value="Cádiz">Cádiz</MenuItem>
                <MenuItem value="Cantabria">Cantabria</MenuItem>
                <MenuItem value="Castellón">Castellón</MenuItem>
                <MenuItem value="Ceuta">Ceuta</MenuItem>
                <MenuItem value="Ciudad Real">Ciudad Real</MenuItem>
                <MenuItem value="Córdoba">Córdoba</MenuItem>
                <MenuItem value="Cuenca">Cuenca</MenuItem>
                <MenuItem value="Formentera">Formentera</MenuItem>
                <MenuItem value="Fuerteventura">Fuerteventura</MenuItem>
                <MenuItem value="Girona">Girona</MenuItem>
                <MenuItem value="Granada">Granada</MenuItem>
                <MenuItem value="Gran Canaria">Gran Canaria</MenuItem>
                <MenuItem value="Guadalajara">Guadalajara</MenuItem>
                <MenuItem value="Guipuzcoa">Guipuzcoa</MenuItem>
                <MenuItem value="Huelva">Huelva</MenuItem>
                <MenuItem value="Huesca"> Huesca</MenuItem>
                <MenuItem value="Ibiza"> Ibiza</MenuItem>
                <MenuItem value="Jaén"> Jaén</MenuItem>
                <MenuItem value="Lanzarote"> Lanzarote</MenuItem>
                <MenuItem value="La Rioja"> La Rioja</MenuItem>
                <MenuItem value="Las Palmas de Gran Canaria">
                  {" "}
                  Las Palmas de Gran Canaria
                </MenuItem>
                <MenuItem value="León"> León</MenuItem>
                <MenuItem value="Lérida"> Lérida</MenuItem>
                <MenuItem value="Lugo"> Lugo</MenuItem>
                <MenuItem value="Madrid"> Madrid</MenuItem>
                <MenuItem value="Málaga"> Málaga</MenuItem>
                <MenuItem value="Mallorca"> Mallorca</MenuItem>
                <MenuItem value="Menorca"> Menorca</MenuItem>
                <MenuItem value="Murcia"> Murcia</MenuItem>
                <MenuItem value="Navarra"> Navarra</MenuItem>
                <MenuItem value="Orense"> Orense</MenuItem>
                <MenuItem value="Palencia"> Palencia</MenuItem>
                <MenuItem value="Pontevedra"> Pontevedra</MenuItem>
                <MenuItem value="Salamanca"> Salamanca</MenuItem>
                <MenuItem value="Santa Cruz de Tenerife">
                  {" "}
                  Santa Cruz de Tenerife
                </MenuItem>
                <MenuItem value="Segovia"> Segovia</MenuItem>
                <MenuItem value="Sevilla"> Sevilla</MenuItem>
                <MenuItem value="Soria"> Soria</MenuItem>
                <MenuItem value="Tarragona"> Tarragona</MenuItem>
                <MenuItem value="Teruel"> Teruel</MenuItem>
                <MenuItem value="Toledo"> Toledo</MenuItem>
                <MenuItem value="Valencia"> Valencia</MenuItem>
                <MenuItem value="Valladolid"> Valladolid</MenuItem>
                <MenuItem value="Vizcaya"> Vizcaya</MenuItem>
                <MenuItem value="Zamora"> Zamora</MenuItem>
                <MenuItem value="Zaragoza">Zaragoza</MenuItem>
              </Select>
              <FormHelperText>{t("register.cityInfo")}</FormHelperText>
            </FormControl>
            {errors.city && (
              <p style={{ color: "red" }}>{errors.city.message}</p>
            )}
          </div>
          <Button color="primary" variant="contained" type="submit">
            {t("register.loginButton")}
          </Button>
          <div>
            <Button
              variant="contained"
              onClick={() => {
                history.push(routes.login);
              }}
              style={{
                backgroundColor: "white",
                marginTop: "20px",
                marginBottom: "20px",
              }}
            >
              {t("register.registerButton")}
            </Button>
          </div>
        </form>
      </div>
    </Container>
  );
};

export default Register;
