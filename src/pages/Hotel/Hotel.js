import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { Container } from "@material-ui/core";
import "./Hotel.css";
import { getHotel } from "../../api/hotels";
import { Card, CardContent, CardMedia, Grid } from "@material-ui/core";
import GoBackButton from "../../components/GoBackButton/GoBackButton";
import {useTranslation} from "react-i18next";

const Hotels = () => {

  const { t } = useTranslation('common');
  const { id: hotelId } = useParams();
  const [hotel, setHotel] = useState([]);
  useEffect(() => {
    getHotel(hotelId).then((hotelList) => setHotel(hotelList));
  }, []);

  return (
    <Container>
      {hotel === null ? (
        <p>Cargando...</p>
      ) : (
        <Grid container spacing={4} justify="center">
          <Grid item xs={10} sm={8} md={8}>
            <Card>
              <CardMedia title="title" />
              <CardContent>
                <div className="hotelsList__card-container">
                  <span>
                    <div>
                      <p style={{ fontWeight: "bold", fontSize: "25px" }}>
                        {hotel.name}
                      </p>
                    </div>
                  </span>
                  <span>
                  <div>
                    <p>{t('planet.climate')}: {hotel.climate}</p>
                  </div>
                  <div>
                    <p>{t('planet.population')}: {hotel.population}</p>
                  </div>
                  </span>
                </div>
                <div className="hotelsList__image-container">
                  <img
                    className="hotel__image"
                    src="https://media.wired.com/photos/5909633776f462691f012e5c/master/pass/tatooine-ft.jpg"
                    alt="star-wars-planet"
                  />
                  <GoBackButton />
                </div>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      )}
    </Container>
  );
};

export default Hotels;
