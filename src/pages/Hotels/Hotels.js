import React, { useState, useEffect } from "react";
import { useHistory, generatePath } from "react-router";
import { Container } from "@material-ui/core";
import routes from "../../config/routes";
import "./Hotels.css";
import { getHotels } from "../../api/hotels";
import { Card, CardContent, CardMedia, Grid } from "@material-ui/core";
import { useTranslation } from "react-i18next";

const Hotels = () => {
  const { t } = useTranslation("common");
  const [hotels, setHotels] = useState([]);
  const [sort, setSort] = useState(true);
  useEffect(() => {
    getHotels().then((hotelList) => setHotels(hotelList));
  }, []);

  const history = useHistory();

  const handleClickOrder = () => {
    setSort(!sort);

    if (sort) {
      hotels.sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });
    } else {
      hotels.sort((a, b) => {
        if (a.name > b.name) {
          return -1;
        }
        if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
    }
  };

  let list = [];
  if (hotels.length > 0) {
    list = hotels.map((hotel) => (
      <Grid
        key={hotel.url.split("/")[5]}
        item
        xs={12}
        sm={12}
        md={6}
        onClick={() => {
          history.push(
            generatePath(routes.hotel, {
              id: hotel.name === "Tatooine" ? 1 : hotel.url.split("/")[5],
            })
          );
        }}
      >
        <Card>
          <CardMedia title="title" />
          <CardContent>
            <div className="hotelsList__card-container">
              <span>
                <div>
                  <p style={{ fontWeight: "bold", fontSize: "25px" }}>
                    {hotel.name}
                  </p>
                </div>
              </span>
              <span>
                <div>
                  <p>
                    {t("planet.climate")}: {hotel.climate}
                  </p>
                </div>
                <div>
                  <p>
                    {t("planet.population")}: {hotel.population}
                  </p>
                </div>
              </span>
            </div>
          </CardContent>
        </Card>
      </Grid>
    ));
  }
  return (
    <Container>
      <button className="hotels__sort-button" onClick={handleClickOrder}>
      {t("hotels.sort")}
      </button>
      <Grid container spacing={8} justify="center">
        {list}
      </Grid>
    </Container>
  );
};

export default Hotels;
