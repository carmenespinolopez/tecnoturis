import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Container, TextField, Button } from "@material-ui/core";
import { useHistory } from "react-router";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { Typography } from "@material-ui/core";
import routes from "../../config/routes";
import "./Login.css";
import login from "../../api/login";
import {useTranslation} from "react-i18next";

const Login = () => {

  const { t } = useTranslation('common');
  const history = useHistory();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = async ({email, password}) => {
    const data = await login(email, password);
    document.location = "/hotels";
  };

  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <Container maxWidth="sm">
      <div className="login__container">
        <Typography variant="h3" gutterBottom color="primary">
          {t('login.title')}
        </Typography>
        <form 
          onSubmit={handleSubmit(onSubmit)} 
          style={{ marginLeft: "50px" }}
        >
          <div>
            <TextField
              color="primary"
              type="text"
              label="Email"
              {...register("email", {
                required: t('errorControl.required'),
                pattern: {
                  value: /^\S+@\S+$/i,
                  message: t('errorControl.email')
                },
              })}
            />
            {errors.email && <p style={{ color: "red" }}>{errors.email.message}</p>}
          </div>
          <div>
            <TextField
              type={showPassword ? "text" : "password"}
              label={t('login.password')}
              {...register("password", {
                required: t('errorControl.required'),
                minLength: {
                  value: 8,
                  message: t('errorControl.password'),
                },
              })}
            />
            <IconButton onClick={handleClickShowPassword}>
              {showPassword ? <Visibility /> : <VisibilityOff />}
            </IconButton>
            {errors.password && <p style={{ color: "red" }}>{errors.password.message}</p>}
          </div>

          <div style={{ marginBottom: "15px" }}>
            <Typography variant="body2">
            {t('login.forgotPwd')}{" "}
            </Typography>
            <Button
              color="primary"
              href="mailto:example@example.com"
              style={{ padding: "0px" }}
            >
              <Typography variant="caption">{t('login.change')}</Typography>
            </Button>
          </div>

          <Button 
            variant="contained"  
            color="primary" 
            type="submit"
            onClick={handleSubmit(onSubmit)}
          >
            {t('login.loginButton')}
          </Button>
        </form>
        <div>
          <Button
            variant="contained"
            onClick={() => {
              history.push(routes.register);
            }}
            style={{ backgroundColor: "white", marginTop: "20px" }}
          >
            {t('login.registerButton')}
          </Button>
        </div>
      </div>
    </Container>
  );
};

export default Login;
