import React, { useState, useEffect } from "react";
import { useHistory, generatePath } from "react-router";
import routes from "../../config/routes";
import { Container } from "@material-ui/core";
import "./Home.css";
import { getHotel } from "../../api/hotels";
import { Card, CardContent, CardMedia, Grid } from "@material-ui/core";
import {useTranslation} from "react-i18next";

const Home = () => {

  const { t } = useTranslation('common');
  let randomNumber = Math.floor(Math.random() * 10) + 1;
  const [hotel, setHotel] = useState([]);
  useEffect(() => {
    getHotel(randomNumber).then((hotel) => setHotel(hotel));
  }, []);

  const history = useHistory();

  return (
    <Container>
      <div className="home__container">
        <p>{t('home.welcome')}</p>
        <p>{t('home.warning')}</p>
        <img
          src="https://i.kym-cdn.com/entries/icons/original/000/000/157/itsatrap.jpg"
          alt="es-una-trampa-star-wars"
          className="home__trap-image"
        />
        <p>
          {t('home.trap')}
        </p>
        <h3>
          {t('home.destiny')}
        </h3>
      </div>
      <Grid container spacing={4} justify="center">
        <Grid 
          item 
          xs={10} 
          sm={8} 
          md={6}
          onClick={() => {
          history.push(
            generatePath(routes.hotel, {
              id: hotel.name === "Tatooine" ? 1 : hotel.url.split("/")[5],
            })
          );
        }}
        >
          <Card>
            <CardMedia title="title" />
            <CardContent>
              <div className="hotelsList__card-container">
                <span>
                  <div>
                    <p style={{ fontWeight: "bold", fontSize: "25px" }}>
                      {hotel.name}
                    </p>
                  </div>
                </span>
                <span>
                  <div>
                    <p>{t('planet.climate')}: {hotel.climate}</p>
                  </div>
                  <div>
                    <p>{t('planet.population')}: {hotel.population}</p>
                  </div>
                </span>
              </div>
              <div className="hotelsList__image-container">
                <img
                  className="hotel__image"
                  src="https://media.wired.com/photos/5909633776f462691f012e5c/master/pass/tatooine-ft.jpg"
                  alt="star-wars-planet"
                />
              </div>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Home;
