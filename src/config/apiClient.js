let baseUrl = 'https://test.api.amadeus.com';
if (process.env.NODE_ENV !== "development") {
 baseUrl = ''; 
}
 
export { baseUrl };
 
const apiClient = (url, config) => {
 return fetch(url, config).then(response => response.json());
};
 
export default apiClient;