const home = `/`;
const hotels = `${home}hotels`;
const hotel = `${hotels}/:id`;
const login = `${home}login`;
const register = `${home}register`;

const routes = {
    home,
    hotels,
    login,
    register,
    hotel
};

export default routes;
