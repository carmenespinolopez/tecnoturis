import { createMuiTheme } from "@material-ui/core/styles";
import colors from "./utils/colors";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: colors.blue,
    },
    secondary: {
      main: colors.cloudyBlue,
    },
    error: {
      main: colors.error,
    },
    warning: {
      main: colors.tiffanyBlue,
    }
  },
  typography: {
    fontFamily: "Barlow",
  
    h1: {
      fontSize: 32,
      fontWeight: 600,
      color: colors.titleH1,
    },
    h2: {
      fontSize: 25,
      fontWeight: 500,
      color: colors.ocean,
    },
    body1: {
      fontSize: 17,
      fontWeight: 400,
    },
    caption: {
      fontSize: 12,
      fontWeight: 400,
    },
  },
  spacing: 4,
});

export default theme;
