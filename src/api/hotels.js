import apiClient from "../config/apiClient";

const getHotels = async () => {
    const requestResult = await apiClient(
        "https://swapi.dev/api/planets",
        {
            "method": "GET"
        }
    );
    return requestResult.results;
}

const getHotel = async (id) => {
    const result = await apiClient(`https://swapi.dev/api/planets/${id}`,
    {
        method: 'GET',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });
    return result;
}


export { getHotels, getHotel };