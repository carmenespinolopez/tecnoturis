import apiClient from "../config/apiClient";

const login = async (email, password) => {
  const body = { email, password };
  const result = await apiClient(`https://reqres.in/api/login`, {
    method: "POST",
    body: body,
  });

  if (result.status === 200) {
    return {
      ok: true,
    };
  }
  return {
    ok: false,
    error: result.error,
  };
};

export default login;
