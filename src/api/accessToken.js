import apiClient from "../config/apiClient";

const getAccessToken = async () => {
    const requestResult = await apiClient(
        `https://test.api.amadeus.com/v1/security/oauth2/token`,
        {
            "method": "POST",
            "body": {
                "client_id": "PWK4cwwiGWYdTjhEaYch54JQMo86m0ae",
                "client_secret": "22tY0VRmfutnu88S",
                "grant_type": "client_credentials",
            }
        }
    );
    return requestResult;
}


export { getAccessToken };