const colors = {
  blue: '#16369F',
  tiffanyBlue: '#6af7dd',
  cloudyBlue: '#afd3db',
  error: "#FF0000"

}

export default colors;