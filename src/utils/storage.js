const TOKEN = 'token';

function saveToken(token) {
    localStorage.setItem(TOKEN, token);
}

function getToken() {
    return localStorage.getItem(TOKEN);
}

function deleteToken() {
    localStorage.removeItem(TOKEN);
}

export default {
    saveToken,
    getToken,
    deleteToken
};
