import React, { useState, useEffect } from 'react';
import './Header.css';
import routes from '../../config/routes';
import { useHistory } from "react-router";
import { withRouter } from 'react-router-dom';
import LangButtons from '../LangButtons';
import {useTranslation} from "react-i18next";

const Header = () => {

  const history = useHistory();
  const { t } = useTranslation('common');

  return (
      <div className="header">
        <span 
          className="header__logo-container"
          onClick={() => {
            history.push(routes.home);
          }}
        >
          LOGO
        </span>
        <span className="header__buttons-container">
          <button 
            className="header__buttons header__buttons-hotels"
            onClick={() => {
              history.push(routes.hotels);
            }}
          >
            {t('header.hotels')}
          </button>
        </span>
        <span className="header__login-container">
          <button 
            className="header__buttons header__buttons-login"
            onClick={() => {
                history.push(routes.login);
            }}
          > 
            {t('header.login')}
          </button>
          <span>
            <LangButtons />
        </span>
        </span>
      </div>  
  );
};

export default withRouter(Header);
