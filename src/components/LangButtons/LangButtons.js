import React, { useState, useEffect } from 'react';
import { useTranslation } from "react-i18next";
import './LangButtons.css';

export default function LangButtons() {
  const { i18n } = useTranslation('common');
  const [showSpanish, setShowSpanish] = useState(false);
  const [showEnglish, setShowEnglish] = useState(true);

  const getLanguage = () => {
    return i18n.language;
  };

  let currentLanguage = getLanguage();

  const english = () => {
    i18n.changeLanguage('en');
    setShowSpanish(true);
    setShowEnglish(false);
  }

  const spanish = () => {
    i18n.changeLanguage('es');
    setShowSpanish(false);
    setShowEnglish(true);
  }

  useEffect(() => {
    currentLanguage = getLanguage();

    if (currentLanguage === 'en') {
      setShowSpanish(true);
      setShowEnglish(false);
    } else if (currentLanguage === 'es') {
      setShowSpanish(false);
      setShowEnglish(true);
    }
    
  }, [currentLanguage])

  return (
    <div className="langButtons__container">
      <button 
        id="esButton" 
        onClick={spanish}
        className={showSpanish ? "langButtons__button" : "hidden"}
      >
        <p className="wordsButton">
          Cambiar a español
        </p>
      </button>
      <button 
        id="enButton" 
        onClick={english}
        className={showEnglish ? "langButtons__button" : "hidden"}
      >
        <p className="wordsButton">
          Switch to English
        </p>
      </button>
    </div>
  )
}
