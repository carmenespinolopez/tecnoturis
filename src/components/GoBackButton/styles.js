import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  cardButton: {
    display: 'flex',
    flexDirection: 'column',
    paddingBottom: '30px'
  }
}));

export default useStyles;
