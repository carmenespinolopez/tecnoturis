import React from 'react';
import { useHistory } from "react-router";
import {
  CardActions,
  Button,
} from "@material-ui/core";
import { Container } from '@material-ui/core';
import routes from '../../config/routes';
import {useTranslation} from "react-i18next";

const GoBackButton = () => {
  const history = useHistory();  
  const { t } = useTranslation('common');

  return (
    <Container>
       <CardActions>
        <Button 
          size="large" 
          color="primary" 
          variant="contained" 
          onClick={() => {history.push(routes.hotels)}}
          style={{ marginBottom: "10px", marginTop: "10px" }}
        >
          {t('planet.goBack')}
        </Button>
      </CardActions>
    </Container>
  )
};

export default GoBackButton;
