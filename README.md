# Tecnoturis test

Application built with ReactJS, MaterialUI as styling library and i18next as a library for web translation..

## Installation
- Clone the repository
```sh
git clone <link>
```
- Go to the location of the cloned repository
```sh
cd tecnoturis
```
- Install the dependencies and start the application
```sh
npm i
npm start
```
- The application will automatically open at `http://localhost:3000/`*
- ✨ Magic ✨

*In case you're currently using the port 3000, the application will ask you if you want to use another port.
Click "y" and it'll open in 'http://localhost:{PORT}'

### More information:
- [Official ReactJS page](https://reactjs.org/)
- [Official MaterialUI page](https://mui.com/)
- [Official i18next page](https://www.i18next.com/)
- [Codeandweb i18next tutorial](https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-react-app-with-react-i18next)

